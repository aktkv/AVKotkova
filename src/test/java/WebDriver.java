import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;

import java.util.HashMap;
import java.util.Map;

public class WebDriver {

    @BeforeClass
    public void openBrowser() {
        WebDriver.setUpDriver("false", "http://217.25.216.158:4444/wd/hub");
    }

    @AfterMethod
    public void closeBrowser() {
        Selenide.closeWebDriver();
    }

    public static void setUpDriver(String start, String hub) {
        if ("true".equals(start)) setRemoteBrowser(hub);
        else setLocalBrowser();
    }

    private static void setLocalBrowser() {
        baseSetUp();
    }

    private static void setRemoteBrowser(String hub) {
        baseSetUp();
        Configuration.remote = hub;
    }
    private static void baseSetUp() {
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--no-sandbox");
        chromeOptions.addArguments("--disable-gpu");
        chromeOptions.addArguments("--remote-allow-origins=*");
        chromeOptions.addArguments("--ignore-ssl-errors");
        chromeOptions.addArguments("--ignore-certificate-errors");
        chromeOptions.addArguments("--disable-dev-shm-usage");


        Map<String, Object> selenoid = new HashMap<>();
        selenoid.put("enableVNC", true);
        chromeOptions.setCapability("selenoid:options", selenoid);

        Configuration.timeout = 30000;
        Configuration.browser = "chrome";
        Configuration.screenshots = false;
        Configuration.savePageSource = false;
        Configuration.browserSize = "1920x1080";
        Configuration.pageLoadStrategy = "normal";
        Configuration.pageLoadTimeout = 50000;
        Configuration.browserCapabilities = chromeOptions;
    }
}
